package com.jtrps.lab5;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{

    @Test
    public void shouldAdd1and1Is2() {
        int result = App.add(1, 1);
        assertEquals(2, result);
    }

    @Test
    public void Add2and1Is3() {
        int result = App.add(2,1);
        assertEquals(3, result);
    }

    @Test
    public void shouldAddMinus1and0isMinus1() {
        int result = App.add(-1,0);
        assertEquals(-1, result);
    }
}
